#!/usr/bin/env bash

#Pretty(ish) print
PURPLISH=$(tput setaf 125)
BLUE=$(tput setaf 4)
BOLD=$(tput bold)
NORMAL=$(tput sgr0)
UNDERLINED_BEG=$(tput smul)
UNDERLINED_END=$(tput rmul)

#authentication details
MOODLE="https://auth.sorbonne-universite.fr/cas/login?service=https%3A%2F%2Fmoodle-sciences.upmc.fr%2Fmoodle-2020%2Flogin%2Findex.php%3FauthCAS%3DCAS"
#MOODLE="https://auth.sorbonne-universite.fr/cas/login?service=https%3A%2F%2Fmoodle-sciences.upmc.fr%2Fmoodle-2020%2Flogin%2Findex.php%3FauthCAS%3DCAS&gateway=true"
#MOODLE="https://auth.sorbonne-universite.fr/cas/login?service=https%3A%2F%2Fmoodle-sciences.upmc.fr%2Fmoodle-2020%2Flogin%2Findex.php%3FauthCAS%3DCAS"
MOODLE_HOMEPAGE="https://moodle-sciences.upmc.fr/moodle-2020/my/"



#temporary files
if [[ -z $XDG_CACHE_HOME ]]; then
  XDG_CACHE_HOME=$HOME/.cache
fi

CACHE_DIR=$XDG_CACHE_HOME/moodlent
if [[ ! -d $CACHE_DIR ]]; then
  mkdir -p $CACHE_DIR
fi

SUCC_CHECC=$CACHE_DIR/succ_checc #to see if you SUCCessfully logged into Moodle
COOKIE_JAR=$CACHE_DIR/cookies.txt
CURL_OUTPUT=$CACHE_DIR/curl_output_moodle_upmc
LINKS_DUMP1=$CACHE_DIR/links_dump1             #to store the first batch of links before redirection
LINKS_DUMP2=$CACHE_DIR/links_dump2             #to store the second one
LINKS_DUMP3=$CACHE_DIR/links_dump3             #with all the links to download the files
MOODLE_LINKS=$CACHE_DIR/moodle_upmc_links      #to store all links redirecting to another Moodle page
NON_MOODLE_LINKS1=$CACHE_DIR/non_moodle_links1 #to store all links redirecting to somewhere outside of Moodle
NON_MOODLE_LINKS2=$CACHE_DIR/non_moodle_links2 #to store the actual links

clear_cache() {
  rm -f "$SUCC_CHECC"
  rm -f "$COOKIE_JAR"
  rm -f "$CURL_OUTPUT"
  rm -f "$LINKS_DUMP1"
  rm -f "$LINKS_DUMP2"
  rm -f "$LINKS_DUMP3"
  rm -f "$MOODLE_LINKS"
  rm -f "$NON_MOODLE_LINKS1"
  rm -f "$NON_MOODLE_LINKS2"
}

check_creds() {
  if [[ $1 = 'checkenv' ]]; then
    if [[ -z $MOODLENT_USERNAME ]]; then
      read -rep "${BOLD}${PURPLISH}Please enter your username (student number): ${NORMAL}" MOODLENT_USERNAME
    fi
    if [[ -z $MOODLENT_PASSWORD ]]; then
      read -resp "${BOLD}${PURPLISH}Please enter your password: ${NORMAL}" MOODLENT_PASSWORD
    fi
  else
    read -rep "${BOLD}${PURPLISH}Please enter your username (student number): ${NORMAL}" MOODLENT_USERNAME
    read -resp "${BOLD}${PURPLISH}Please enter your password: ${NORMAL}" MOODLENT_PASSWORD
  fi
  echo
}

#script
if [[ $1 = '-c' ]]; then # TODO: look into getopts if more command-line arguments come along
  clear_cache
  echo -e "${BOLD}All temporary files cleared${NORMAL}"
  exit 0
elif [[ $1 = '-C' ]]; then # TODO: ditto
  rm -rf $CACHE_DIR
  echo -e "${BOLD}Cache dir deleted${NORMAL}"
  exit 0
fi

#0. Ask for auth details if the variables are unset
check_creds checkenv

for ((i = 0; ; i++)); do
  #1. connect to the page
  curl -s -L "$MOODLE" -c "$COOKIE_JAR" -b "$COOKIE_JAR" -o "$CURL_OUTPUT"

  #2. Grab hidden input value
  EXECUTION_HIDDEN_VALUE=$(grep -o 'execution" value=".*"' "$CURL_OUTPUT" | cut -d '/' -f 1 | grep -o '=".*' | sed 's/["=]//g')

  #3. finally try to connect
  curl -L -X POST "$MOODLE" -o "$SUCC_CHECC" -d "username=$MOODLENT_USERNAME&password=$MOODLENT_PASSWORD&execution=$EXECUTION_HIDDEN_VALUE&_eventId=submit&geolocation=" -c "$COOKIE_JAR" -b "$COOKIE_JAR"
  curl -s -L "$MOODLE_HOMEPAGE" -c "$COOKIE_JAR" -b "$COOKIE_JAR" -o "$SUCC_CHECC"


  #check if you managed to auth into Moodle
  if ! grep -iwq apereo "$SUCC_CHECC" && ! grep -iqw "Moodle Sciences 2020: Choix du mode d'authentification" "$SUCC_CHECC" "$CURL_OUTPUT"; then
    break
  fi
  if grep -iqw "Moodle Sciences 2020: Choix du mode d'authentification" "$SUCC_CHECC" "$CURL_OUTPUT" && [[ $i -le 15 ]]; then 
    continue
  fi
  read -rep "${BOLD}${PURPLISH}It seems a problem occured during the authentication process. Do you want to try again? (y/N)${NORMAL} " REPLY
  if [[ $REPLY =~ ^[yY]$ ]]; then
    read -rep "${BOLD}${BLUE}Do you want to change your username/password? ${NORMAL} (y/N)" CHANGE_CREDS
    if [[ $CHANGE_CREDS =~ [yY]$ ]]; then
      # read -rep "${BOLD}${PURPLISH}Please enter your username (student number): ${NORMAL}" MOODLENT_USERNAME
      # read -resp "${BOLD}${PURPLISH}Please enter your password: ${NORMAL}" PASSWORD
      check_creds
    fi
  else
    exit 1
  fi
done

#4.Choose your course
echo -e "${BOLD}${PURPLISH}courses: ${NORMAL}${BLUE} \n Discrete Maths: dm \n OOP/Java: oop \n Advanced C: ac \n Functional Programming: fun" "${NORMAL}" # TODO: to generalize
read -re CHOOSE_COURSE
case "$CHOOSE_COURSE" in

  "dm")
    COURSE_PAGE="https://moodle-sciences.upmc.fr/moodle-2020/course/view.php?id=1755"
    ;;
  "oop")
    COURSE_PAGE="https://moodle-sciences.upmc.fr/moodle-2020/course/view.php?id=2403"
    ;;
  "ac")
    COURSE_PAGE="https://moodle-sciences.upmc.fr/moodle-2020/course/view.php?id=2494"
    ;;
  "fun")
    COURSE_PAGE="https://moodle-sciences.upmc.fr/moodle-2020/course/view.php?id=3399"
    ;;
  *)
    echo -e "${BOLD}${PURPLISH}No corresponding course could be found ${NORMAL}"
    ;;
esac
#5. Engage in kopimism to acquire the desired files
curl -s -L "$COURSE_PAGE" -c "$COOKIE_JAR" -b "$COOKIE_JAR" -o "$CURL_OUTPUT"

#to get files to which you're redirected to
#shellcheck disable=2129
sed 's/>/\'$'\n/g' "$CURL_OUTPUT" | grep -o 'https://moodle-sciences.upmc.fr/moodle-2020/mod/resource/.*"' | sed 's/"//g' >>"$LINKS_DUMP1"
sed 's/>/\'$'\n/g' "$CURL_OUTPUT" | grep -o 'https://moodle-sciences.upmc.fr/moodle-2020/mod/folder/.*"' | sed 's/"//g' >>"$LINKS_DUMP1"
#to get links redirecting to somewhere outside of Moodle
sed 's/>/\'$'\n/g' "$CURL_OUTPUT" | grep -o 'https://moodle-sciences.upmc.fr/moodle-2020/mod/url/.*"' | sed 's/"//g' >>"$NON_MOODLE_LINKS1"
#to get all links redirecting to another Moodle page
sed 's/>/\'$'\n/g' "$CURL_OUTPUT" | grep -o 'https://moodle-sciences.upmc.fr/moodle-2020/mod/page/.*"' | sed 's/"//g' >>"$MOODLE_LINKS"
#to get files where a direct link is available:cleans them up and puts them directly in the final link list
sed 's/>/\'$'\n/g' "$CURL_OUTPUT" | grep 'href="https://moodle-sciences.upmc.fr/moodle-2020/pluginfile.php' | grep -o '="h.*"' | sed 's/[="]//g' | sed 's/?.*//g' >>"$LINKS_DUMP3"

#to gather all the links to the actual files from the redirection headers (for presumably downloadable files)
while IFS= read -re LINE; do
  curl -s -c "$COOKIE_JAR" -b "$COOKIE_JAR" "$LINE" >>"$LINKS_DUMP2"
done <"$LINKS_DUMP1"

#to gather all the actual links to non-Moodle pages
while IFS= read -re LINE; do
  curl -s -c "$COOKIE_JAR" -b "$COOKIE_JAR" "$LINE" >>"$NON_MOODLE_LINKS2"
done <"$NON_MOODLE_LINKS1"

#to clean up and put the links to the files in an easily exploitable list
sed 's/>/\'$'\n/g' "$LINKS_DUMP2" | grep -o '="h.*' | grep plugin | grep -v ' ' | sed 's/?.*//g' | sed 's/[="]//g' >>"$LINKS_DUMP3"

echo -e "${BOLD}${BLUE}Links redirecting to Moodle pages${NORMAL}:"
nl -b a "$MOODLE_LINKS"

echo -e "${BOLD}${BLUE}Links redirecting to web pages outside of Moodle${NORMAL}:"
grep urlworkaround "$NON_MOODLE_LINKS2" | grep -no '="h.*"' | sed 's/[="]//g'

echo -e "${BOLD}${BLUE}Presumably downloadable files${NORMAL}:"
nl -b a "$LINKS_DUMP3"

read -rep "${BOLD}${PURPLISH}Do you want to download any of these files?(y/N) ${NORMAL}" REPLY

if [[ $REPLY =~ ^[yY]$ ]]; then
  while :; do
    read -rep "${BOLD}${PURPLISH}Where do you want to put them (they'll be put in the current directory if you don't specify anything): ${NORMAL}" DESTINATION
    DESTINATION=${DESTINATION:-$(pwd)}
    #shellcheck disable=2164
    if [[ -d $(realpath -e "$DESTINATION") ]]; then
      cd "$DESTINATION"
      break
    else
      read -rep "${BOLD}${PURPLISH}This destination does not seem to exist or is not a directory. Do you want to try again (y/N)? ${NORMAL}" NEW_DEST
      if [[ ! $NEW_DEST =~ ^[yY]$ ]]; then
        break
      fi
    fi
  done
  echo -e "${BOLD}${PURPLISH}Your files will be put in $(pwd)${NORMAL}"

  read -rep "${BOLD}${PURPLISH}Do you want to download all of them (y/N)?${NORMAL} " REPLY
  if [[ $REPLY =~ ^[yY]$ ]]; then
    while IFS= read -re LINE; do
      curl -c "$COOKIE_JAR" -b "$COOKIE_JAR" -O "$LINE"
    done <"$LINKS_DUMP3"
  else
    read -rep "${BOLD}${PURPLISH}Please enter the line number(s) ${UNDERLINED_BEG}(separated by a space)${UNDERLINED_END} of the files you wish to download: ${NORMAL}" -a FILES
    for i in "${FILES[@]}"; do
      LINE=$(awk "NR==$i" "$LINKS_DUMP3")
      curl -c "$COOKIE_JAR" -b "$COOKIE_JAR" -O "$LINE"
    done
  fi
    fi
