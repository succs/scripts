#!/usr/bin/env bash
#Pretty(ish) print
PURPLISH=$(tput setaf 125)
BLUE=$(tput setaf 4)
BOLD=$(tput bold)
NORMAL=$(tput sgr0)
UNDERLINED_BEG=$(tput smul)
UNDERLINED_END=$(tput rmul)

#authentication details

#Set the variables to optimise laziness
MOODLE="https://cassal-2.paris-sorbonne.fr/cas/login?service=https%3A%2F%2Fmoodle.paris-sorbonne.fr%2Flogin%2Findex.php%3FauthCAS%3DCAS"
#USERNAME=
#PASSWORD=
#temporary files
SUCC_CHECC=$HOME/.succ_checc #to see if you SUCCessfully logged into Moodle
COOKIE_JAR=$HOME/.cookieJar
CURL_OUTPUT=$HOME/.curl_output_moodle_lettre.html
LINKS_DUMP1=$HOME/.links_dump1 #to store the first batch of links before redirection
LINKS_DUMP2=$HOME/.links_dump2 #to store the second one
LINKS_DUMP3=$HOME/.links_dump3 #with all the links to download the files
rm "$SUCC_CHECC"
rm "$COOKIE_JAR"
rm "$CURL_OUTPUT"
rm "$LINKS_DUMP1"
rm "$LINKS_DUMP2"
rm "$LINKS_DUMP3"

#script
#0.Ask for auth details if the variables are unset

if [[ -z $USERNAME ]]; then
  read -rep "${BOLD}${PURPLISH}Please enter your username (student number): ${NORMAL}" USERNAME
fi

if [[ -z $PASSWORD ]]; then
  read -resp "${BOLD}${PURPLISH}Please enter your password: ${NORMAL}" PASSWORD
fi
while :; do
  #1.connect to the page
  curl -s -L "$MOODLE" -c "$COOKIE_JAR" -b "$COOKIE_JAR" -o "$CURL_OUTPUT"

  #2.Grab hidden input values
  LT_HIDDEN_VALUE=$(grep -o '"lt" value=".*"' "$CURL_OUTPUT" | grep -o 'LT.*fr')
  EXECUTION_HIDDEN_VALUE=$(grep -o 'execution" value=".*"' "$CURL_OUTPUT" | grep -o '=".*"' | sed 's/["=]//g')

  #3.Finally try to connect
  curl -s -L -X POST -o "$SUCC_CHECC" "$MOODLE" -d "username=$USERNAME&password=$PASSWORD&lt=$LT_HIDDEN_VALUE&execution=$EXECUTION_HIDDEN_VALUE&_eventId=submit&submit=SE+CONNECTER" -c $COOKIE_JAR -b $COOKIE_JAR
  #check if you managed to auth into Moodle
  if ! grep -F 'Central Authentication Service' "$SUCC_CHECC"; then
    break
  fi
  read -rep "${BOLD}${PURPLISH}It seems a problem occured during the authentication process. Do you want to try again? (y/N)${NORMAL} " REPLY
  if [[ $REPLY =~ ^[yY]$ ]]; then
    read -rep "${BOLD}${BLUE}Do you want to change your username/password? ${NORMAL} (y/N)" CHANGE_CREDS
    if [[ $CHANGE_CREDS =~ [yY]$ ]]; then
      read -rep "${BOLD}${PURPLISH}Please enter your username (student number): ${NORMAL}" USERNAME
      read -resp "${BOLD}${PURPLISH}Please enter your password: ${NORMAL}" PASSWORD
    fi
  else
    exit 1
  fi
done

#4.Choose your course
echo -e "${BOLD}${PURPLISH}courses: ${NORMAL}${BLUE} \n EPF: TD (tde) ou CM (cme)  \n LittClass: TD (tdl) ou CM (cml) \n English (en)\n GramStyl: TD (tdg) ou CM (cmg)" "${NORMAL}"
read -re CHOOSE_COURSE

case "$CHOOSE_COURSE" in
"cme")
  COURSE_PAGE="https://moodle.paris-sorbonne.fr/course/view.php?id=9252"
  ;;
"tde")
  COURSE_PAGE="https://moodle.paris-sorbonne.fr/course/view.php?id=9553"
  ;;
"cml")
  COURSE_PAGE="https://moodle.paris-sorbonne.fr/course/view.php?id=9307"
  ;;
"tdl")
  COURSE_PAGE="https://moodle.paris-sorbonne.fr/course/view.php?id=7380"
  ;;
"en")
  COURSE_PAGE="https://moodle.paris-sorbonne.fr/course/view.php?id=9565"
  COURSE_PAGE1="https://moodle.paris-sorbonne.fr/course/view.php?id=9824"
  ;;
"tdg")
  COURSE_PAGE="https://moodle.paris-sorbonne.fr/course/view.php?id=7753"
  ;;
"cmg")
  COURSE_PAGE="https://moodle.paris-sorbonne.fr/course/view.php?id=5221"
  ;;
*)
  echo -e "${BOLD}${PURPLISH}No corresponding course could be found ${NORMAL}"
  ;;
esac

#5. Engage in kopimism to acquire the desired files
curl -s -L "$COURSE_PAGE" -c "$COOKIE_JAR" -b "$COOKIE_JAR" -o "$CURL_OUTPUT"
if [[ -n $COURSE_PAGE1 ]]; then
  curl -s -L "$COURSE_PAGE1" -c "$COOKIE_JAR" -b "$COOKIE_JAR" >>"$CURL_OUTPUT"
fi
#to get files to which you're redirected to
sed 's/>/\'$'\n/g' "$CURL_OUTPUT" | grep -o 'https://moodle.paris-sorbonne.fr/mod/resource/.*"' | sed 's/"//g' >>"$LINKS_DUMP1"
#to get files where a direct link is available (mostly audio files): clean them up directly and dumps them in the final link list file
sed 's/>/\'$'\n/g' "$CURL_OUTPUT" | grep 'href="https://moodle.paris-sorbonne.fr/pluginfile.php' | grep -o '="h.*"' | sed 's/[="]//g' >>"$LINKS_DUMP3"

#to gather all the links to the actual files from the redirection headers
while IFS= read -re LINE; do
  curl -s -c "$COOKIE_JAR" -b "$COOKIE_JAR" "$LINE" >>"$LINKS_DUMP2"
done <"$LINKS_DUMP1"

#to clean up and put the links to the files in an easily exploitable list
sed 's/>/\'$'\n/g' "$LINKS_DUMP2" | grep -o '="h.*' | sed 's/[="]//g' | grep plugin >>"$LINKS_DUMP3"
nl -b a "$LINKS_DUMP3"

read -rep "${BOLD}${PURPLISH}Do you want to download any of these files?(y/N) ${NORMAL}" REPLY

if [[ $REPLY =~ ^[yY]$ ]]; then
  while :; do
    read -rep "${BOLD}${PURPLISH}Where do you want to put them (they'll be put in the current directory if you don't specify anything): ${NORMAL}" DESTINATION
    DESTINATION=${DESTINATION:-$(pwd)}
    #shellcheck disable=2164
    if [[ -d $(realpath -e "$DESTINATION") ]]; then
      cd "$DESTINATION"
      break
    else
      read -rep "${BOLD}${PURPLISH}This destination does not seem to exist or is not a directory. Do you want to try again (y/N)? ${NORMAL}" NEW_DEST
      if [[ ! $NEW_DEST =~ ^[yY]$ ]]; then
        break
      fi
    fi
  done
  echo -e "${BOLD}${PURPLISH}Your files will be put in $(pwd)${NORMAL}"

  read -rep "${BOLD}${PURPLISH}Do you want to download all of them (y/N)?${NORMAL} " REPLY
  if [[ $REPLY =~ ^[yY]$ ]]; then
    while IFS= read -re LINE; do
      curl -c "$COOKIE_JAR" -b "$COOKIE_JAR" -O "$LINE"
    done <"$LINKS_DUMP3"
  else
    read -rep "${BOLD}${PURPLISH}Please enter the line number(s) ${UNDERLINED_BEG}(separated by a space)${UNDERLINED_END} of the files you wish to download: ${NORMAL}" -a FILES
    for i in "${FILES[@]}"; do
      LINE=$(awk "NR==$i" "$LINKS_DUMP3")
      curl -c "$COOKIE_JAR" -b "$COOKIE_JAR" -O "$LINE"
    done
  fi
fi
