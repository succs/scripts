#!/usr/bin/env bash

mag="\e[35m"
blu="\e[34m"
yel="\e[33m"
grn="\e[32m"
red="\e[31m"
b="\e[1m"
c="\e[0m"

usage() {
  echo -e "
  ${b}Usage:$c $(basename $0) [OPTS] <nom de fichier>

$b${yel}Options$c
  $b-h$c: afficher ce message d'aide à l'usage
  $b-u$c: spécifier l'URL à rechercher manuellement (normalement non-requise)
  $b-c$c: spécifier une UE manuellement
  $b-g$c: spécifier votre groupe/section
  $b-1$c: soumettre votre TME tout seul

$b${yel}Variables d'environnement$c
  Vous pouvez sauvegarder vos préférences en définissant les variables d'environnement appropriées.
  Regardez le script pour plus de détails.
  
$b${grn}Script made by:$c
  - chilledfrogs <${blu}chilledfrogs@disroot.org$c>
  - Speykious    <${blu}speykious@gmail.com$c>
"
}

IFS=' '

OPTIND=1

while getopts "h:u:c:g:1" opt; do
  case "$opt" in
    h) usage && exit ;;
    u) url=$OPTARG ;;
    c) SUBMIT_COURSE=$OPTARG ;;
    g) SUBMIT_GROUP=$OPTARG ;;
    1) SUBMIT_1=1 ;;
    *) echo -e "${red}Option invalide:$c $opt" >&2 && usage && exit 1
  esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

if [[ $# -ne 1 ]]; then
  usage && exit 1
fi

if ! ls "$1" &> /dev/null; then
  echo -e "$b${red}ERREUR:$c${red}Fichier introuvable$c" >&2
  usage && exit 1
fi

echo -e "${b}Soumission de votre TME d'informatique$c"

if [[ -z $SUBMIT_COURSE ]]; then
  courseRegex='LU[[:digit:]]{1}IN[[:digit:]]{3}'
  if [[ $PWD =~ $courseRegex ]]; then
    SUBMIT_COURSE=$(echo "$PWD" | grep -Eo "$courseRegex" | head -n 1)
    echo -e "${blu}UE détectée:$c $SUBMIT_COURSE"
  elif [[ $1 =~ $courseRegex ]]; then
    SUBMIT_COURSE=$(echo "$1" | grep -Eo "$courseRegex" | head -n 1)
    echo -e "${blu}UE détectée:$c $SUBMIT_COURSE"
  else
    echo -ne "${blu}UE non détectée automatiquement, veuillez la renseigner: $c"
    read -r SUBMIT_COURSE
  fi
fi

year=$(date +%Y)
fall="oct"
spring="feb"
timeofyear="$year$fall"
url="http://www.licence.info.upmc.fr/lmd/licence/$year/ue/$SUBMIT_COURSE-$timeofyear/cgi-bin/remettreDevoir$year.cgi?t=$(date +%s)"

if ! curl -sf -o /dev/null "$url"; then
  timeofyear="$((year+1))$spring"
fi

url="http://www.licence.info.upmc.fr/lmd/licence/$year/ue/$SUBMIT_COURSE-$timeofyear/cgi-bin/remettreDevoir$year.cgi?t=$(date +%s)"

if ! curl -sf -o /dev/null "$url"; then
  echo -e "$b${red}Attention:$c$red je ne trouve pas de script de soumission pour cette UE. Procédez avec attention.$c"
else
  groups=$(curl -s "$url" | grep option | grep -v "???" | grep -o '".*"' | tr -d '"')
  if [[ -z $SUBMIT_GROUP ]]; then
    echo -e "${blu}Voici les groupes que j'ai trouvé pour cette UE:$c"
    echo "$groups"
  else
    echo "$groups" | grep "$SUBMIT_GROUP" &> /dev/null || echo -e "$b${red}Attention:$c$red je ne trouve pas le groupe spécifié. Procédez avec attention.$c"
  fi
fi

if [[ -z $SUBMIT_GROUP ]]; then
  echo -ne "${blu}Entrez votre section et/ou groupe: $c"
  read -r SUBMIT_GROUP
fi

echo -e "$b${blu}Étudiant n°1$c"

if [[ -z $SUBMIT_ID1 ]]; then
  echo -ne "  ${blu}Entrez votre numéro d'étudiant [$(whoami)]: $c"
  read -r SUBMIT_ID1
  SUBMIT_ID1=${SUBMIT_ID1:-$(whoami)}
fi

if [[ -z $SUBMIT_SURNAME1 ]]; then
  echo -ne "  ${blu}Entrez votre nom (de famille): $c"
  read -r SUBMIT_SURNAME1
fi

if [[ -z $SUBMIT_NAME1 ]]; then
  echo -ne "  ${blu}Entrez votre prénom: $c"
  read -r SUBMIT_NAME1
fi

if [[ -z $SUBMIT_EMAIL1 ]]; then
  default="$(echo "$SUBMIT_NAME1" | tr '[:upper:]' '[:lower:]' | tr ' ' '_').$(echo "$SUBMIT_SURNAME1" | tr '[:upper:]' '[:lower:]' | tr ' ' '_')"
  if [[ -z $SUBMIT_UPMC_EMAIL ]]; then
    default="$default@etu.sorbonne-universite.fr"
  else
    default="$default@etu.upmc.fr"
  fi
  echo -ne "  ${blu}Entrez votre adresse mail [$default]: $c"
  read -r SUBMIT_EMAIL1
  SUBMIT_EMAIL1=${SUBMIT_EMAIL1:-$default}
fi

if [[ -z $SUBMIT_1 ]]; then
  echo -e "$b${blu}Étudiant n°2$c"

  if [[ -z $SUBMIT_ID2 ]]; then
    echo -ne "  ${blu}Entrez votre numéro d'étudiant: $c"
    read -r SUBMIT_ID2
  fi

  if [[ -z $SUBMIT_SURNAME2 ]]; then
    echo -ne "  ${blu}Entrez votre nom (de famille): $c"
    read -r SUBMIT_SURNAME2
  fi

  if [[ -z $SUBMIT_NAME2 ]]; then
    echo -ne "  ${blu}Entrez votre prénom: $c"
    read -r SUBMIT_NAME2
  fi

  if [[ -z $SUBMIT_EMAIL2 ]]; then
    default="$(echo "$SUBMIT_NAME2" | tr '[:upper:]' '[:lower:]' | tr ' ' '_').$(echo "$SUBMIT_SURNAME2" | tr '[:upper:]' '[:lower:]' | tr ' ' '_')"
    if [[ -z $SUBMIT_UPMC_EMAIL ]]; then
      default="$default@etu.sorbonne-universite.fr"
    else
      default="$default@etu.upmc.fr"
    fi
    echo -ne "  ${blu}Entrez votre adresse mail [$default]: $c"
    read -r SUBMIT_EMAIL2
    SUBMIT_EMAIL2=${SUBMIT_EMAIL2:-$default}
  fi
fi

echo -e "\n$b${yel}Envoi de la requête POST en cours...$c"
if ! curl -s -F section="$SUBMIT_GROUP" -F id1="$SUBMIT_ID1" -F id2="$SUBMIT_ID2" -F nom1="$SUBMIT_SURNAME1" -F nom2="$SUBMIT_SURNAME2" -F prenom1="$SUBMIT_NAME1" -F prenom2="$SUBMIT_NAME2" -F mel1="$SUBMIT_EMAIL1" -F mel2="$SUBMIT_EMAIL2" -F fichier=@"$1" -F action=Envoyer "$url" | grep "SUCC&Egrave;S" &> /dev/null; then
  echo -e "$b${red}ÉCHEC$c
${red}Une erreur semble s'être produite, je ne trouve pas de message de succès.$c"
  which w3m &> /dev/null && echo -e "${mag}Vous semblez avoir w3m sur cette machine ($HOSTNAME).
Pour tenter de régler le problème, essayez de taper la commande suivante:$c
  
  ${b}w3m$c '$url'
"
  exit 1
else
  echo -e "$b${grn}YEET$c
${grn}TME envoyé avec succès. Vérifiez vos mails!$c"
fi
