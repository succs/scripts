#!/usr/bin/env bash

#Pretty(ish) print
PURPLISH=$(tput setaf 125)
BLUE=$(tput setaf 4)
BOLD=$(tput bold)
NORMAL=$(tput sgr0)
UNDERLINED_BEG=$(tput smul)
UNDERLINED_END=$(tput rmul)

#authentication details
MOODLE="https://auth.sorbonne-universite.fr/cas/login?service=https%3A%2F%2Fmoodle-sciences.upmc.fr%2Fmoodle-2021%2Flogin%2Findex.php%3FauthCAS%3DCAS"
#MOODLENT_USERNAME=
#MOODLENT_PASSWORD=

#temporary files
if [[ -z $XDG_CACHE_HOME ]]; then
  XDG_CACHE_HOME=$HOME/.cache
fi

CACHE_DIR=$XDG_CACHE_HOME/moodlent
if [[ ! -d $CACHE_DIR ]]; then
  mkdir -p "$CACHE_DIR"
fi

SUCC_CHECC=$CACHE_DIR/.succ_checc #to see if you SUCCessfully logged into Moodle
COOKIE_JAR=$CACHE_DIR/.cookieJar
CURL_OUTPUT=$CACHE_DIR/.curl_output_moodle_upmc
LINKS_DUMP0=$CACHE_DIR/.links_dump0             #all initial links
LINKS_DUMP1=$CACHE_DIR/.links_dump1             #to store the first batch of links before redirection
LINKS_DUMP2=$CACHE_DIR/.links_dump2             #to store the second one
DIRECT_LINKS_TO_FILES=$CACHE_DIR/.links_dump3   #with all the links to download the files
MOODLE_LINKS=$CACHE_DIR/.moodle_umpc_links      #to store all links redirecting to another Moodle page
NON_MOODLE_LINKS1=$CACHE_DIR/.non_moodle_links1 #to store all links redirecting to somewhere outside of Moodle
NON_MOODLE_LINKS2=$CACHE_DIR/.non_moodle_links2 #to store the actual links
NON_MOODLE_LINKS3=$CACHE_DIR/.non_moodle_links3 #to store all final non-moodle links

#helper functions
DEP="pup"
check_deps() {
  if ! [ "$(command -v "$DEP")" ]; then
    echo "${BOLD}${PURPLISH}missing dependancy (pup): please install it following the instructions at: https://github.com/EricChiang/pup${NORMAL}"
    echo -e "If you have Go installed on your computer, you can run:"
    echo "go get github.com/ericchiang/pup"
    echo "if you are on MacOS and have homebrew, please run:"
    echo "brew install pup"
    exit 1
  fi

}

clear_cache() {
  rm "$SUCC_CHECC"
  rm "$COOKIE_JAR"
  rm "$CURL_OUTPUT"
  rm "$LINKS_DUMP0"
  rm "$LINKS_DUMP1"
  rm "$LINKS_DUMP2"
  rm "$DIRECT_LINKS_TO_FILES"
  rm "$MOODLE_LINKS"
  rm "$NON_MOODLE_LINKS1"
  rm "$NON_MOODLE_LINKS2"
  rm "$NON_MOODLE_LINKS3"
}

check_cred() {
  if [[ $1 = 'checkenv' ]]; then
    if [[ -z $MOODLENT_USERNAME ]]; then
      read -rep "${BOLD}${PURPLISH}Please enter your username (student number): ${NORMAL}" MOODLENT_USERNAME
    fi
    if [[ -z $MOODLENT_PASSWORD ]]; then
      read -resp "${BOLD}${PURPLISH}Please enter your password: ${NORMAL}" MOODLENT_PASSWORD
    fi
  else
    read -rep "${BOLD}${PURPLISH}Please enter your username (student number): ${NORMAL}" MOODLENT_USERNAME
    read -resp "${BOLD}${PURPLISH}Please enter your password: ${NORMAL}" MOODLENT_PASSWORD
  fi
  echo
}

#script
#clear cache on option #creds don't work that way currently
if [[ $1 = '-c' ]]; then # TODO: look into getopts if more command-line arguments come along
  clear_cache
  echo -e "${BOLD}All temporary files cleared${NORMAL}"
  exit 0
elif [[ $1 = '-C' ]]; then # TODO: ditto
  rm -rf "$CACHE_DIR"
  echo -e "${BOLD}Cache dir deleted${NORMAL}"
  exit 0
fi

#check if all dependancies are installed
check_deps

###############clearing cache by default for the moment
clear_cache

#0. Ask for auth details if the variables are unset
check_cred checkenv

for ((i = 0; ; i++)); do
  #1. connect to the page
  curl -s -L "$MOODLE" -c "$COOKIE_JAR" -b "$COOKIE_JAR" -o "$CURL_OUTPUT"

  #2. Grab hidden input value
  EXECUTION_HIDDEN_VALUE=$(pup -f "$CURL_OUTPUT" 'input[name="execution"] attr{value}')
  #should do the same in case pup fails
  #EXECUTION_HIDDEN_VALUE=$(grep -o 'execution" value=".*"' "$CURL_OUTPUT" | cut -d '/' -f 1 | grep -o '=".*' | sed 's/["=]//g' | awk -F">" 'FNR == 1{print $1;}')
  #echo "hidden value: $EXECUTION_HIDDEN_VALUE"

  #3. finally try to connect
  curl -L -X POST "$MOODLE" -o "$SUCC_CHECC" -c "$COOKIE_JAR" -b "$COOKIE_JAR" -d "username=$MOODLENT_USERNAME&password=$MOODLENT_PASSWORD&execution=$EXECUTION_HIDDEN_VALUE&_eventId=submit&geolocation=&submit=LOGIN"

  #3.check if you managed to auth into Moodle
  if grep -iwq "tableau de bord" "$SUCC_CHECC"; then
    break
  fi
  if grep -iqw "Moodle Sciences 2021: Choix du mode d'authentification" "$SUCC_CHECC" "$CURL_OUTPUT" && [[ $i -le 15 ]]; then
    continue
  fi
  read -rep "${BOLD}${PURPLISH}It seems a problem occured during the authentication process. Do you want to try again? (y/N)${NORMAL} " REPLY
  if [[ $REPLY =~ ^[yY]$ ]]; then
    read -rp "${BOLD}${BLUE}Do you want to change your username/password? ${NORMAL} (y/N)" CHANGE_CREDS
    if [[ $CHANGE_CREDS =~ [yY]$ ]]; then
      #read -rp "${BOLD}${PURPLISH}Please enter your username (student number): ${NORMAL}" MOODLENT_USERNAME
      #read -rsp "${BOLD}${PURPLISH}Please enter your password: ${NORMAL}" MOODLENT_PASSWORD
      check_cred
    fi
  else
    exit 1
  fi
done

#4. find out which courses you're following
#store course names into array
mapfile -t COURSE_CODE_ARR < <(pup -c 'li a[data-parent-key="mycourses"] .media-body text{}' -f "$SUCC_CHECC")
#store course links into array
mapfile -t COURSE_LINK_ARR < <(pup -c 'li a[data-parent-key="mycourses"] attr{href}' -f "$SUCC_CHECC")

#5.Choose your course
echo "${BOLD}${PURPLISH}courses: ${NORMAL}"
for i in "${!COURSE_CODE_ARR[@]}"; do
  echo "${BLUE}enter $i for ${COURSE_CODE_ARR[$i]}${NORMAL}"
done

while :; do
  read -r CHOOSE_COURSE
  #echo "${COURSE_LINK_ARR[$CHOOSE_COURSE]}"

  if [ "${COURSE_LINK_ARR[$CHOOSE_COURSE]}" ]; then
    COURSE_PAGE="${COURSE_LINK_ARR[$CHOOSE_COURSE]}"
    break
  else
    echo -e "${BOLD}${PURPLISH}No corresponding course could be found ${NORMAL}"
  fi
done

#6. Engage in kopimism to acquire the desired files
curl -s -L "$COURSE_PAGE" -c "$COOKIE_JAR" -b "$COOKIE_JAR" -o "$CURL_OUTPUT"

#7 extract all links in homepage of the course
pup -c 'a attr{href}' -f "$CURL_OUTPUT" >>"$LINKS_DUMP0"

#to get files to which you're redirected to
grep '/resource' "$LINKS_DUMP0" >>"$LINKS_DUMP1"
grep '/folder' "$LINKS_DUMP0" >>"$LINKS_DUMP1"

#to get links redirecting to somewhere outside of Moodle
grep '/url' "$LINKS_DUMP0" >>"$NON_MOODLE_LINKS1"
grep -v 'moodle' "$LINKS_DUMP0" | grep ".*/.*" >>"$NON_MOODLE_LINKS3"

#to get all links redirecting to another Moodle page
grep '/page/' "$LINKS_DUMP0" >>"$MOODLE_LINKS"

#to get files where a direct link is available:cleans them up and puts them directly in the final link list
grep '/pluginfile' "$LINKS_DUMP0" >>"$DIRECT_LINKS_TO_FILES"
nl -b a "$DIRECT_LINKS_TO_FILES"

#sed 's/>/\'$'\n/g' "$CURL_OUTPUT" | grep 'href="https://moodle-sciences.upmc.fr/moodle-2020/pluginfile.php' | grep -o '="h.*"' | sed 's/[="]//g' | sed 's/?.*//g' >>"$DIRECT_LINKS_TO_FILES"
#to gather all the links to the actual files from the redirection headers (for presumably downloadable files)

# to only write out final urls after redirections
WRITE_OPTION="%{url_effective}\n"
#discard all other output
CURL_DUMP="/dev/null"
echo "redirections------------------------------"
while IFS= read -r LINE; do
  curl -o "$CURL_DUMP" -Ls -I -c "$COOKIE_JAR" -b "$COOKIE_JAR" --write-out "$WRITE_OPTION" "$LINE" >>"$DIRECT_LINKS_TO_FILES"
  #curl -s -c "$COOKIE_JAR" -b "$COOKIE_JAR" "$LINE" >>"$LINKS_DUMP2"
done <"$LINKS_DUMP1"

#to gather all the actual links to non-Moodle pages
echo "non moodle links------------------------------"
while IFS= read -r LINE; do
  curl -o "$CURL_DUMP" -Ls -I -c "$COOKIE_JAR" -b "$COOKIE_JAR" --write-out "$WRITE_OPTION" "$LINE" >>"$NON_MOODLE_LINKS2"
  #curl -s -c "$COOKIE_JAR" -b "$COOKIE_JAR" "$LINE" >>"$NON_MOODLE_LINKS2"
done <"$NON_MOODLE_LINKS1"
#add them to the already existing list
grep ".*/.*" "$NON_MOODLE_LINKS2" >>"$NON_MOODLE_LINKS3"

#to clean up and put the links to the files in an easily exploitable list

#sed 's/>/\'$'\n/g' "$LINKS_DUMP2" | grep -o '="h.*' | grep plugin | grep -v ' ' | sed 's/?.*//g' | sed 's/[="]//g' >>"$DIRECT_LINKS_TO_FILES"

echo -e "${BOLD}${BLUE}Links redirecting to Moodle pages${NORMAL}:"
nl -b a "$MOODLE_LINKS"

echo -e "${BOLD}${BLUE}Links redirecting to web pages outside of Moodle${NORMAL}:"
#grep urlworkaround "$NON_MOODLE_LINKS2" | grep -no '="h.*"' | sed 's/[="]//g'
nl -b a "$NON_MOODLE_LINKS3"

echo -e "${BOLD}${BLUE}Presumably downloadable files${NORMAL}:"
nl -b a "$DIRECT_LINKS_TO_FILES"

read -rp "${BOLD}${PURPLISH}Do you want to download any of these files?(y/N) ${NORMAL}" REPLY

if [[ $REPLY =~ ^[yY]$ ]]; then
  while :; do
    read -rp "${BOLD}${PURPLISH}Where do you want to put them (they'll be put in the current directory if you don't specify anything): ${NORMAL}" DESTINATION
    DESTINATION=${DESTINATION:-$(pwd)}
    #shellcheck disable=2164
    if [[ -d $(realpath -e "$DESTINATION") ]]; then
      cd "$DESTINATION"
      break
    else
      read -rp "${BOLD}${PURPLISH}This destination does not seem to exist or is not a directory. Do you want to try again (y/N)? ${NORMAL}" NEW_DEST
      if [[ ! $NEW_DEST =~ ^[yY]$ ]]; then
        break
      fi
    fi
  done
  echo -e "${BOLD}${PURPLISH}Your files will be put in $(pwd)${NORMAL}"

  read -rp "${BOLD}${PURPLISH}Do you want to download all of them (y/N)?${NORMAL} " REPLY
  if [[ $REPLY =~ ^[yY]$ ]]; then
    while IFS= read -r LINE; do
      curl -c "$COOKIE_JAR" -b "$COOKIE_JAR" -O "$LINE"
    done <"$DIRECT_LINKS_TO_FILES"
  else
    read -rp "${BOLD}${PURPLISH}Please enter the line number(s) ${UNDERLINED_BEG}(separated by a space)${UNDERLINED_END} of the files you wish to download: ${NORMAL}" -a FILES
    for i in "${FILES[@]}"; do
      LINE=$(awk "NR==$i" "$DIRECT_LINKS_TO_FILES")
      curl -c "$COOKIE_JAR" -b "$COOKIE_JAR" -O "$LINE"
    done
  fi
fi
